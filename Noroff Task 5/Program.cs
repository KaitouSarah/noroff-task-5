﻿using System;
using System.Collections.Generic;
/*
Program for creating a contact list, and letting the user search for 
contacts by name only. Will include partial hits.
*/
namespace Noroff_Task_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Run();
        }

        //Method to run the program, calling methods for generating contact list and searching in it.
        static void Run()
        {
            //Generates a list of contacts.
            List<Person> contactList = generateContactList();

            //Prompt user for search string and save.
            Console.Write("Search for a contact by name: ");
            string searchCriteria = Console.ReadLine().ToLower();

            //Search the contact list for the string from the user.
            search(contactList, searchCriteria);

        }

        //Method searches for a name based on the contact list and search string (NOTE: Case sensitive).
        static void search(List<Person> contactList, string searchCriteria)
        {
            //Keeps track on wheather there are any hits in the search at all.
            bool noHits = true;

            Console.WriteLine($"Your search for \"{searchCriteria}\" resulted in the following hits:\n ");

            //Looping through the contact list to see if there are any matches for the user search.
            foreach(Person contact in contactList) {
                if (contact.GetFullName().Contains(searchCriteria)) {
                    contact.PrintContactInfo();
                    noHits = false;
                }
            }

            if (noHits)
            {
                //Just a friendly note that your search had no results.
                Console.WriteLine("Noone, you have no friends!");
            }
        }

        //Brilliant method that generates a hard coded list of contacts.
        static List<Person> generateContactList()
        {
            List<Person> contactList = new List<Person>();

            //Creating some people.
            Person contact1 = new Person("David", "Draiman", "91929394", "david@draiman.com");
            Person contact2 = new Person("Ivan", "Moody", "92939495", "ivan@moody.com");
            Person contact3 = new Person("Matthew", "Shadows", "93949596", "matthew@shadows.com");
            Person contact4 = new Person("James", "Hetfield", "94959697", "james@hetfield.com");
            Person contact5 = new Person("The", "Rev", "95969798", "the@rev.com");

            //Adding said people to contact list.
            contactList.Add(contact1);
            contactList.Add(contact2);
            contactList.Add(contact3);
            contactList.Add(contact4);
            contactList.Add(contact5);

            return contactList;
        }
    }
}
