# Noroff Task 5 AND 10

Task 5: Name Search
 Write a program which stores a sample of 5 contact
names
 First and Last name
 Then allows the user to search the list by name and
determine if the name is in the list
 Partial matches should work
 Display all possible matches

Task 10: Upgraded Name Search
 Modify your name search solution to have the following:
 A separate class that describes a person
 Reminder: It will need its own source file
 First Name, Last Name, Telephone, etc
 Use overloaded constructors*
 Create a collection of this new class
 Modify your search to access a public method within this
class

